<?php

namespace Fluens\ActivityModels;

use Illuminate\Support\ServiceProvider;

class ActivityModelsServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        include __DIR__.'/Activity.php';
        include __DIR__.'/InfoElement.php';
        include __DIR__.'/InfoFile.php';
        include __DIR__.'/InfoFileElement.php';
        include __DIR__.'/InfoValue.php';
        include __DIR__.'/Participation.php';
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
