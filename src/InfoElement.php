<?php

namespace Fluens\ActivityModels;

use Illuminate\Database\Eloquent\Model;

class InfoElement extends Model
{
    protected $fillable = ['application_id','infoable_type'];
}
