<?php

namespace Fluens\ActivityModels;

use Illuminate\Database\Eloquent\Model;

class InfoValue extends Model
{
    public function infoElement(){
        return $this->hasOne('App\InfoElement');
    }

    public function infoable(){
        return $this->morphTo();
    }
}
