<?php

namespace Fluens\ActivityModels;

use Illuminate\Database\Eloquent\Model;

class InfoFile extends Model
{
    protected $fillable = ['infoable_type'];

    public function infoable(){
        return $this->morphTo();
    }

    public function infoFileElement(){
        return $this->belongsTo('App\InfoFileElement');
    }
}
