<?php

namespace Fluens\ActivityModels;

use Illuminate\Database\Eloquent\Model;

class Activity extends Model
{
    protected $fillable = ['start','end','numberOfSeats','name'];

    /*public function location(){
        return $this->belongsTo(\App\Location::class)
    } TODO uncommenten zodra location bestaat*/

    public function infoElements(){
        return $this->morphMany('App\InfoValue','infoable');
    }

    public function infoFiles(){
        return $this->morphMany('App\InfoFile','infoable');
    }

    public function participationRoles(){
        return $this->belongsToMany('App\ParticipationRole','audience_type')
            ->withPivot('quota');
        //om participationrole met quota te linken:
        //$activity->participationRoles()->attach($participationRole,['quota' => 50])
    }

    public function participations(){
        return $this->hasMany('App\Participation');
    }

    public function prices(){
        return $this->hasMany('App\Price');
    }

    public function engagements(){
        return $this->hasMany('App\Engagement');
    }

    public function tags(){
        return $this->morphToMany('App\Tag','taggable');
    }
}
