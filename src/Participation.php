<?php

namespace App;

use App\Events\AvailableSpot;
use App\Events\ParticipationCanceled;
use App\Events\ParticipationOpened;
use Illuminate\Database\Eloquent\Model;
use Jedrzej\Searchable\SearchableTrait;
use Laravel\Scout\Searchable;
use TomLingham\Searchy\Facades\Searchy;

/**
 * @property ParticipationRole $participation_role
 * @property Activity $activity
 * @property Price $price
 * @property Status $status
 * @property Status $formerStatus
 * @property Status $nextStatus
 * @property string name
 */
class Participation extends Model
{

    use Searchable;

    /**
     * Get the indexable data array for the model.
     *ph
     * @return array
     */
    public function toSearchableArray()
    {
        return [
            'id' => $this->id,
            'activity.name' => $this->activity->name,
            'status.name' => $this->status->name,
        ];
    }

    protected $fillable = [
        'application_id' , 'activity_id' , 'participant_id' , 'participation_role_id' , 'price_id' , 'status_id',
    ];

    public function participationRole(){
        return $this->belongsTo(ParticipationRole::class);
    }

    public function activity(){
        return $this->belongsTo(Activity::class);
    }

    public function invoice(){
        return $this->belongsTo('App\Invoice');
    }

    public function price(){
        return $this->belongsTo(Price::class);
    }
    public function status(){
        return $this->belongsTo(Status::class);
    }

    public function updateStatus(Status $status){
        if($status->statusable_type !== Participation::class){
            return response(null,400);
        }

        //controleren of statusverandering een vrijgekomen plaats is bij een volle participationtype
        if($this->status->name == "Ingeschreven" && $status->name == "Geannuleerd"){
            $participationType = ParticipationType::all()
                ->where('activity_id', $this->activity()->id)
                ->where('participation_role_id',$this->participationRole()->id)->first();
            $participations = Participation::all()->where('activity_id',$this->activity()->id)
                ->where('participation_role_id',$this->participationRole()->id);
            //event firen indien ja
            if(count($participations) == $participationType->quota){
                event(new AvailableSpot($participationType));
            }
        }
        $this->status_id = $status->id;
        $this->save();
        return true;
    }


    /* @author henokv @date 19/04/18 11:32 */
    public static function openable(){
        return Participation::whereHas('activity', function($query){
            $query->whereDate('start', now()->toDateString());
        })->get();
    }
    /* @author henokv @date 19/04/18 11:32 */
    public static function cancelable(){
        return Participation::whereHas('activity', function($query){
            $query->whereDate('end', '<', now()->toDateString());
        })->get();
    }

    /* @author henokv @date 19/04/18 11:32 */
    public function open(){
        if($this->status->name != 'Ingeschreven') return false;
        $this->updateStatus($this->nextStatus);
        event(new ParticipationOpened($this));
        return true;
    }
    /* @author henokv @date 19/04/18 11:32 */
    public function cancel(){
        if($this->status->name != 'Geannuleerd') return false;
        $status = Status::where('statusable_type', Participation::class)->where('name', 'Geannuleerd')->get();
        $this->updateStatus($status);
        event(new ParticipationCanceled());
        return true;

    }
    /* @author henokv @date 19/04/18 11:36 */
    public function getFormerStatusAttribute(){
        return $this->status->formerStatus;
    }
    /* @author henokv @date 19/04/18 11:37 */
    public function getNextStatusAttribute(){
        return $this->status->nextStatus;
    }

}
