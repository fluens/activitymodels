<?php

namespace Fluens\ActivityModels;

use Illuminate\Database\Eloquent\Model;

class InfoFileElement extends Model
{
    protected $fillable = ['fileType','folderPath','infoable_type'];
}
